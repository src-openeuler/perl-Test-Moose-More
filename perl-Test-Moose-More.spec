Name:           perl-Test-Moose-More
Version:        0.050
Release:        1
Summary:        More tools for testing Moose packages
License:        LGPLv2+
URL:            https://metacpan.org/release/Test-Moose-More
Source0:        https://cpan.metacpan.org/authors/id/R/RS/RSRCHBOY/Test-Moose-More-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  make
BuildRequires:  perl-generators
BuildRequires:  perl-interpreter
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
# Run-time:
BuildRequires:  perl(Carp)
BuildRequires:  perl(Data::OptList)
BuildRequires:  perl(List::MoreUtils)
BuildRequires:  perl(List::Util) >= 1.45
BuildRequires:  perl(Moose::Util)
BuildRequires:  perl(Moose::Util::TypeConstraints)
BuildRequires:  perl(Scalar::Util)
BuildRequires:  perl(Sub::Exporter::Progressive)
BuildRequires:  perl(Syntax::Keyword::Junction)
BuildRequires:  perl(Test::Builder)
BuildRequires:  perl(Test::Moose)
BuildRequires:  perl(Test::More) >= 0.94
# Tests only:
BuildRequires:  perl(blib)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(IO::Handle)
BuildRequires:  perl(IPC::Open3)
BuildRequires:  perl(Moose)
BuildRequires:  perl(Moose::Deprecated)
BuildRequires:  perl(Moose::Meta::Attribute)
BuildRequires:  perl(Moose::Role)
BuildRequires:  perl(Moose::Util::MetaRole)
BuildRequires:  perl(namespace::autoclean)
BuildRequires:  perl(TAP::SimpleOutput) >= 0.009
BuildRequires:  perl(Test::Builder::Tester)
BuildRequires:  perl(Test::CheckDeps) >= 0.010
Requires:       perl(Test::More) >= 0.94

# Removed under-specified dependencies
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}^perl\\(Test::More\\)$

%description
This package contains a number of additional tests that can be employed
against Moose classes/roles. It is intended to replace Test::Moose.

%prep
%setup -q -n Test-Moose-More-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
make %{?_smp_mflags}

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%license LICENSE
%doc Changes README
%{perl_vendorlib}/*
%{_mandir}/man3/*

%changelog
* Wed Mar 20 2024 konglidong <konglidong@uniontech.com> - 0.050-1
- package init
